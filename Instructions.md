Source: https://css-tricks.com/gulp-for-beginners/

**1. Installing and running default task**

Goto this url: https://github.com/gulpjs/gulp/blob/v3.9.1/docs/getting-started.md

**2. Setup a settings file called ackage.json inside the project folder.**

Run `npm init` and fill in the data.

**3. Install gulp-sass to project.**

Run `npm install gulp-sass --savedev` to install gulp-sass. `--savedev` also places gulp-sass as dev dependency the project.

**4. Require gulp-sass in project.**

In gulpfile.js, add the following statement.

`var sass = require('gulp-sass');`

**5. Make a task to convert sass to css.**

`gulp.task('sass', function () {
    // convert sass to css
    return gulp.src('scss/*.scss')            // Source file for SASS
        .pipe(sass())                       // Convert sass to css
        .pipe(gulp.dest('css/'))            // Destination folder for CSs
});`

To Convert SASS to CSS, run the following function.

`gulp sass`

**6. Add a watcher to run `sass()` when file changes.**

`gulp.task('watch',function () {
    gulp.watch('scss/*.scss',['sass']);
});`

Run the task using `gulp watch`.


**7. Install browsersync.**

`npm install browser-sync --save-dev`

**8. Add brower-sync as variable.**

`var browserSync=require('browser-sync').create();`

**9. Add a browsersync task**

`gulp.task('browserSync', function () {
     browserSync.init({
         server: {
             baseDir: './'
         },
     });
 });`
 
 **10. Reload browser on watcher task as well.**
 
 `gulp.task('watch', ['browserSync', 'sass'], function () {
      gulp.watch('scss/**/*.scss', ['sass']);  // Track all sass files, and run sass whenever any one file changes.
      gulp.watch('./*.html', browserSync.reload); // Reload browser if html files changes.
      gulp.watch('./*.php', browserSync.reload); // Reload browser if php files changes.
      gulp.watch('./scripts/**/*.js', browserSync.reload);     // Reload browser if javascript files changes.
  });`


