var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

gulp.task('sass', function () {
    // convert sass to css
    return gulp.src('scss/*.scss')                      // Source file for SASS - Style.scss is tracked
        .pipe(sass({outputStyle: 'compressed'}))        // Convert sass to css
        .pipe(gulp.dest('css/'))                        // Destination folder for CSS
        .pipe(browserSync.reload(
            {
                stream: true
            }
        ))
});

gulp.task('browserSync', function () {
    browserSync.init({
        server: {
            baseDir: './'
        },
    });
});

gulp.task('watch', ['browserSync', 'sass'], function () {
    gulp.watch('scss/**/*.scss', ['sass']);  // Track all sass files, and run sass whenever any one file changes.
    gulp.watch('./*.html', browserSync.reload); // Reload browser if html files changes.
    gulp.watch('./*.php', browserSync.reload); // Reload browser if php files changes.
    gulp.watch('./scripts/**/*.js', browserSync.reload);     // Reload browser if javascript files changes.
});



